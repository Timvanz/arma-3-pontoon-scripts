// This script allows pontoons to be loaded on the back of a truck.
// The pontoons will apear one on top of the other.
// Usage (in init of 3CB MAN Truck):
// null = [this] execVM "truck.sqf";
// Maximum number of pontoons can be determined by adjusting cargo size of the
// pontoon and maximum cargo capacity of the truck.

if (isServer) then {

// Select truck itself.
_truck = _this select 0;
// Count the number of pontoons present.
_truck setVariable ["n_pontoons", 0, true];
// Keep track of the pontoon objects loaded in.
_truck setVariable ["pontoons", [], true];
};